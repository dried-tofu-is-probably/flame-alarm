# 火焰报警器

#### 介绍
当火焰传感器感受到火焰，蜂鸣器会响，LED灯亮红灯，当没有火焰，蜂鸣器不会响，LED灯亮绿灯。

#### 软件架构
软件架构说明


#### 代码

#include "stm32f10x.h"                  // Device header
#include "Delay.h"
#include "OLED.h"
#include "AD.h"
#include "Serial.h"
#include "FlameSensor.h"


#define BUZZER_PIN GPIO_Pin_12
#define LED2_PIN GPIO_Pin_14
#define LED4_PIN GPIO_Pin_15
#define BUZZER_GPIO_PORT GPIOB
#define LED_GPIO_PORT GPIOB

// 打印信息，打印出火焰传感器的状态值
void makerobo_Print(int x)
{
  
}

void GPIO_Config(void)
{
    GPIO_InitTypeDef GPIO_InitStructure;

    // 使能GPIOB的时钟
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB, ENABLE);

    // 配置蜂鸣器和LED的GPIO引脚为输出模式
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;

    // 配置蜂鸣器引脚
    GPIO_InitStructure.GPIO_Pin = BUZZER_PIN;
    GPIO_Init(BUZZER_GPIO_PORT, &GPIO_InitStructure);

    // 配置LED引脚
    GPIO_InitStructure.GPIO_Pin = LED2_PIN | LED4_PIN;
    GPIO_Init(LED_GPIO_PORT, &GPIO_InitStructure);
}

int main(void)
{
    int makerobo_analogVal;              // 模拟量变量
    int makerobo_tmp, makerobo_status;   // 模拟量状态值

    OLED_Init();
    AD_Init();
    FlameSensor_Init();
    Serial_Init();
    OLED_ShowString(1, 1, "Value:");
    GPIO_Config(); // 调用GPIO配置函数

    makerobo_status = 0;
    while (1)
    {
        makerobo_analogVal = AD_GetValue();
        printf("Photoresistor Value: %d\n", makerobo_analogVal); // 打印出该值
        OLED_ShowNum(1, 7, makerobo_analogVal, 4);

        makerobo_tmp = FlameSensor_Get();
        if (makerobo_tmp != makerobo_status) // 判断状态发生改变
        {
            makerobo_Print(makerobo_tmp); // 打印出火焰信息值
            makerobo_status = makerobo_tmp; // 把当前状态值作为下次状态值判断，避免重复打印

            // 根据火焰状态控制蜂鸣器和LED
            if (makerobo_tmp == 0) // 有火焰
            {
               GPIO_ResetBits(BUZZER_GPIO_PORT, BUZZER_PIN); // 蜂鸣器响
                GPIO_SetBits(LED_GPIO_PORT, LED2_PIN); // 点亮LED2
                GPIO_ResetBits(LED_GPIO_PORT, LED4_PIN); // 熄灭LED4
            }
            else // 无火焰
            {
                GPIO_SetBits(BUZZER_GPIO_PORT, BUZZER_PIN); // 蜂鸣器不响
                GPIO_ResetBits(LED_GPIO_PORT, LED2_PIN); // 熄灭LED2
                GPIO_SetBits(LED_GPIO_PORT, LED4_PIN); // 点亮LED4
            }
        }

        Delay_ms(200);
    }
}

#### 使用说明

1.  使用smt32基础板进行连线
2.  使用keil5，将上述代码复制进去
3.  将代码下载到板子里

#### 图片
![输入图片说明](3fb6df4a9c55883c51e801fba003f55.jpg)

